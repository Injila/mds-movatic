import time
import json
from flask import Flask, request, render_template, run
import os

app = Flask(__name__)

app.run(host='0.0.0.0')

@app.route('/time')
def get_current_time():
    return {'time': time.time()}

@app.route('/gbfs')
def get_policy():
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url = os.path.join(SITE_ROOT, "static", "station_status.json")
    data = json.load(open(json_url))
    return data