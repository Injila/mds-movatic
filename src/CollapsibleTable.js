import React , { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';



const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function createData(station_name, docks_available, bikes_available, is_returning, is_renting, last_reported, electric_bike, smart_bike, classic_bike) {
  return {
    station_name,
    docks_available,
    bikes_available,
    is_returning,
    is_renting,
    more_information: [
      { data: last_reported, electric_bike: electric_bike, smart_bike: smart_bike, classic_bike: classic_bike }
    ],
  };
}

let rows = [
  createData('bcycle_madison_1874', 9, 0, 1, 1, 3.99)
];

function Row(props) {
  //const [currentData, setCurrentData] = useState({});

  useEffect(() => {
    fetch('/gbfs').then(res => res.json()).then(data => {
      //setCurrentData(data.data);
      for(var i=0; i<data.data.stations.length; i++) {
        rows.push(createData(data.data.stations[i].station_id, data.data.stations[i].num_docks_available, data.data.stations[i].num_bikes_available, data.data.stations[i].is_returning, data.data.stations[i].is_renting, new Date(data.data.stations[i].last_reported).toISOString(), data.data.stations[i].num_bikes_available_types.electric, data.data.stations[i].num_bikes_available_types.smart, data.data.stations[i].num_bikes_available_types.classic ))
      }
    });
  }, []);
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.station_name}
        </TableCell>
        <TableCell align="right">{row.docks_available}</TableCell>
        <TableCell align="right">{row.bikes_available}</TableCell>
        <TableCell align="right">{row.is_returning}</TableCell>
        <TableCell align="right">{row.is_renting}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                More information
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Last reported</TableCell>
                    <TableCell>Electric bikes</TableCell>
                    <TableCell align="right">Smart bikes</TableCell>
                    <TableCell align="right">Classic bikes</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.more_information.map((infoRow) => (
                    <TableRow key={infoRow.date}>
                      <TableCell component="th" scope="row">
                        {infoRow.date}
                      </TableCell>
                      <TableCell>{infoRow.electric_bike}</TableCell>
                      <TableCell align="right">{infoRow.smart_bike}</TableCell>
                      <TableCell align="right">
                      {infoRow.classic_bike}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    calories: PropTypes.number.isRequired,
    carbs: PropTypes.number.isRequired,
    fat: PropTypes.number.isRequired,
    history: PropTypes.arrayOf(
      PropTypes.shape({
        amount: PropTypes.number.isRequired,
        customerId: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
      }),
    ).isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    protein: PropTypes.number.isRequired,
  }).isRequired,
};


export default function CollapsibleTable() {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Station</TableCell>
            <TableCell align="right">Docks available</TableCell>
            <TableCell align="right">Bikes available</TableCell>
            <TableCell align="right">Returning</TableCell>
            <TableCell align="right">Renting</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}